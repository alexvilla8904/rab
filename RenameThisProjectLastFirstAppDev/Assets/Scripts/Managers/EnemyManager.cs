﻿using UnityEngine;

namespace Nightmare
{
    public class EnemyManager : PausibleObject
    {
        private PlayerHealth playerHealth;
        public GameObject enemy;
        public float spawnTime = 3f;
        public Transform[] spawnPoints;
        //public int spawnLimit = 4;
        //private int counter;

        private float timer;
        private int spawned = 0;
        void Start ()
        {
            timer = spawnTime;
            InvokeRepeating("Spawn", spawnTime, spawnTime);
            //repeater();

        }
        //void repeater()
        //{
        //    InvokeRepeating("Spawn", spawnTime, spawnTime);
        //}
        void OnEnable()
        {
            //playerHealth = FindObjectOfType<PlayerHealth>();
            StartPausible();
        }

        void OnDestroy()
        {
            StopPausible();
        }

        //void Update()
        //{
        //    if (isPaused)
        //        return;

        //    timer -= Time.deltaTime;
        //    if (timer <= 0f)
        //    {
        //        Spawn();
        //        timer = spawnTime;
        //    }
        //}

        void Spawn ()
        {           
            // If the player has no health left...
            //if(playerHealth.currentHealth <= 0f)
            //{
            //    // ... exit the function.
            //    return;
            //}

            // Find a random index between zero and one less than the number of spawn points.
            int spawnPointIndex = Random.Range (0, spawnPoints.Length);

            // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
            
            Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
            //counter++;
            //Cancel();

        }
        //void Cancel()
        //{
        //    //counter++;
        //    if(counter >= spawnLimit)
        //    {
        //        CancelInvoke("repeater");
        //    }
        //}
    }
}