﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Nightmare
{
    public class GameOverManager : MonoBehaviour
    {
        private PlayerHealth playerHealth;
        Animator anim;
        Player_Controller player_Controller;

        LevelManager lm;
        private UnityEvent listener;

        void Awake ()
        {
            playerHealth = FindObjectOfType<PlayerHealth>();
            anim = GetComponent <Animator> ();
            lm = FindObjectOfType<LevelManager>();
            EventManager.StartListening("GameOver", ShowGameOver);
        }

        void OnDestroy()
        {
            EventManager.StopListening("GameOver", ShowGameOver);
        }

        void ShowGameOver()
        {
            anim.SetBool("GameOver", true);
        }

        private void ResetLevel()
        {
            //ScoreManager.score = 0;
            //player_Controller.count = 0;
            //LevelManager lm = FindObjectOfType<LevelManager>();
            //lm.LoadInitialLevel();
            //Application.LoadLevel(Application.loadedLevel);
            //SceneManager.LoadScene("BackUpRollABall");
            anim.SetBool("GameOver", false);
            playerHealth.ResetPlayer();
        }
    }
}