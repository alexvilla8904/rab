﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.EventSystems;

public class Camera_Controller : MonoBehaviour
{
    public GameObject player;
    //public Button leftButton;
    //public Button rightButton;
    public Vector3 offset ;
    public float Xpitch = 1f;
    public float Camspeed = 2f;
    private float adjustSpeed = 100f;
    private float yawInput = 1f;

    // Start is called before the first frame update
    void Start()
    {
       //offset = transform.position - player.transform.position;

    }

    // Update is called once per frame
    public void Update()
    {
#if PC
#elif MOBILE

#endif
        //Enhanced controls added below
        yawInput -= Input.GetAxis("CameraHorizontal") * adjustSpeed * Time.deltaTime;

        //Mobile control Code below
        //yawInput -= Input.acceleration.x * adjustSpeed * Time.deltaTime;
        //float moveHorizontal = Input.acceleration.x;
        //float moveVertical = Input.acceleration.y;

    }
    public void LateUpdate()
    {
        //mobile control code below.
        //float moveHorizontal = Input.acceleration.x;
        //float moveVertical = Input.acceleration.y;

        transform.position = player.transform.position + offset;
        transform.LookAt(player.GetComponent<Transform>());
        transform.RotateAround(player.transform.position, Vector3.up, yawInput);

    }

    //public void updatePosition()
    //{
    //EventSystem.current.currentSelectedGameObject.name;
    //  yawInput -= Input.GetAxis("CameraHorizontal") * adjustSpeed * Time.deltaTime;
    //}
}
