﻿#define MOBILE
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Globalization;
using System.Diagnostics;
using System.Security.Cryptography;

public class Player_Controller : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 0f;
    public TextMeshProUGUI countText;
    private Rigidbody rb;
    private int count;
    //internal int count;
    private float movementX;
    private float movementY;
    public GameObject winTextObject;
    ParticleSystem system;
    Nightmare.PlayerHealth PlayerHealth;

    //Below are the new fields added during enhanced controller
    public Transform cam;
    //public Text countText;
    public TextMeshProUGUI winText;
    //public GameObject pickUps;
    private int winCount;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        setCountText();
        winTextObject.SetActive(false);
        //Enhanced Controls below 
        //winCount = pickUps.transform.childCount; *Removed for testing
        //SetWinText("");
    }
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;

    }
    void setCountText()
    {
        countText.text = "Count: " +count.ToString();
        if(count >= 2)
        {
            winTextObject.SetActive(true);
            //PlayerHealth.RestartLevel();
            count = 0;

        }
    }
    private void FixedUpdate()
    {
        //Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        //rb.AddForce(movement * speed);
        //Enhanced Update Below
#if PC
#elif MOBILE


#endif
        float moveHorizontal = Input.GetAxis("CameraHorizontal");
        float moveVertical = Input.GetAxis("CameraVertical");
        //float moveHorizontal = Input.acceleration.x;
        //float moveVertical = Input.acceleration.y; 

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        movement = cam.TransformDirection(movement);
        movement.y = 0f;
        rb.AddForce(movement * speed);
        UnityEngine.Debug.DrawRay(transform.position, movement * speed * rb.drag);
    }
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            AudioSource aud = other.gameObject.GetComponent<AudioSource>();
            system = other.gameObject.GetComponent<ParticleSystem>();
            system.Play();
            aud.Play();
            StartCoroutine(delayDestroy(other.gameObject));
            //Destroy(other.gameObject);
            count++;
            setCountText();
        }

        
    }
    IEnumerator delayDestroy(GameObject g)
    {
     
        yield return new WaitForSeconds(1.3f);
        Destroy(g);
    }
}
